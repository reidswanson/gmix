"""
Documentation
"""
# Python Modules
import unittest

# 3rd Party Modules
import numpy as np

# Project Modules
from cde import umix


class TestQsm(unittest.TestCase):
    n_bins = 5
    data = np.array([
        6.52257717, 2.34982263, 6.727788,   4.15138986, 0.51320004,
        2.32535868, 0.96828099, 8.41226572, 7.74380124, 0.01337345,
        7.03927746, 0.37604815, 2.73762926, 2.92722946, 5.17021558,
        2.0734382,  9.29720181, 6.77436563, 1.0368877,  2.12465936
    ])
    weights = np.array([
        [0.02268327, 0.39942464, 0.06547898, 0.14641103, 0.36600208],
        [0.49006102, 0.01009653, 0.11968732, 0.17448506, 0.20567007]
    ])

    @classmethod
    def setUpClass(cls):
        pass

    def test_make_bins(self):
        act_bins = umix.make_bins(self.data, self.n_bins)
        exp_bins = np.array([0.01337345, 2.33433054, 4.65528763, 6.97624472, 9.29720181])

        self.assertTrue(np.allclose(act_bins, exp_bins))

    def test_bin(self):
        bins = umix.make_bins(self.data, self.n_bins)

        act_bins = umix.get_bin(self.data, bins)
        exp_bins = np.array([
            [0, 0, 0, 1, 0],
            [0, 0, 1, 0, 0],
            [0, 0, 0, 1, 0],
            [0, 0, 1, 0, 0],
            [0, 1, 0, 0, 0],
            [0, 1, 0, 0, 0],
            [0, 1, 0, 0, 0],
            [0, 0, 0, 0, 1],
            [0, 0, 0, 0, 1],
            [0, 1, 0, 0, 0],
            [0, 0, 0, 0, 1],
            [0, 1, 0, 0, 0],
            [0, 0, 1, 0, 0],
            [0, 0, 1, 0, 0],
            [0, 0, 0, 1, 0],
            [0, 1, 0, 0, 0],
            [0, 0, 0, 0, 1],
            [0, 0, 0, 1, 0],
            [0, 1, 0, 0, 0],
            [0, 1, 0, 0, 0],
        ])
        self.assertTrue(np.array_equal(act_bins, exp_bins))

    def test_pdf(self):
        bins = umix.make_bins(self.data, self.n_bins)

        act_pdf = umix.pdf(-1, bins, self.weights)
        exp_pdf = np.array([0.02268327, 0.49006102])
        np.testing.assert_allclose(act_pdf, exp_pdf)

        act_pdf = umix.pdf(1, bins, self.weights)
        exp_pdf = np.array([0.39942464, 0.01009653])
        np.testing.assert_allclose(act_pdf, exp_pdf)

        act_pdf = umix.pdf(3, bins, self.weights)
        exp_pdf = np.array([0.06547898, 0.11968732])
        np.testing.assert_allclose(act_pdf, exp_pdf)

        act_pdf = umix.pdf(5, bins, self.weights)
        exp_pdf = np.array([0.14641103, 0.17448506])
        np.testing.assert_allclose(act_pdf, exp_pdf)

        act_pdf = umix.pdf(7, bins, self.weights)
        exp_pdf = np.array([0.36600208, 0.20567007])
        np.testing.assert_allclose(act_pdf, exp_pdf)

        act_pdf = umix.pdf(10, bins, self.weights)
        exp_pdf = np.array([0.36600208, 0.20567007])
        np.testing.assert_allclose(act_pdf, exp_pdf)

    def test_cdf(self):
        bins = umix.make_bins(self.data, self.n_bins)

        act_cdf = umix.cdf(-1, bins, self.weights)
        exp_cdf = np.array([0.02268327, 0.49006102])
        np.testing.assert_allclose(act_cdf, exp_cdf)

        act_cdf = umix.cdf(1, bins, self.weights)
        exp_cdf = np.array([0.42210791, 0.50015755])
        np.testing.assert_allclose(act_cdf, exp_cdf)

        act_cdf = umix.cdf(3, bins, self.weights)
        exp_cdf = np.array([0.48758689, 0.61984487])
        np.testing.assert_allclose(act_cdf, exp_cdf)

        act_cdf = umix.cdf(5, bins, self.weights)
        exp_cdf = np.array([0.63399792, 0.79432993])
        np.testing.assert_allclose(act_cdf, exp_cdf)

        act_cdf = umix.cdf(7, bins, self.weights)
        exp_cdf = np.array([1, 1])
        np.testing.assert_allclose(act_cdf, exp_cdf)

        act_cdf = umix.cdf(10, bins, self.weights)
        exp_cdf = np.array([1, 1])
        np.testing.assert_allclose(act_cdf, exp_cdf)

    def test_ppf(self):
        bins = umix.make_bins(self.data, self.n_bins)

        act_ppf = umix.ppf(0.6, bins, self.weights)
        exp_ppf = np.array([5.81576618, 3.49480908])
        np.testing.assert_allclose(act_ppf, exp_ppf)

        act_ppf = umix.ppf(0.65, bins, self.weights)
        exp_ppf = np.array([8.13672327, 5.81576618])
        np.testing.assert_allclose(act_ppf, exp_ppf)

        act_ppf = umix.ppf(0.3, bins, self.weights)
        exp_ppf = np.array([1.173852, -1.1471051])
        np.testing.assert_allclose(act_ppf, exp_ppf)

        self.assertRaises(ValueError, umix.ppf, -1.0, bins, self.weights)
        self.assertRaises(ValueError, umix.ppf, +1.1, bins, self.weights)

    def test_mean(self):
        bins = umix.make_bins(self.data, self.n_bins)

        act_mean = umix.mean(bins, self.weights)
        exp_mean = np.array([5.661710350403731, 3.716707970749507])
        np.testing.assert_allclose(act_mean, exp_mean)

    def test_mode(self):
        bins = umix.make_bins(self.data, self.n_bins)

        act_mode = umix.mode(bins, self.weights)
        exp_mode = np.array([2.33433054, 0.01337345])
        np.testing.assert_allclose(act_mode, exp_mode)

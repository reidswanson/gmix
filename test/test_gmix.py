"""
Documentation
"""
# Python Modules
import logging
import unittest

# 3rd Party Modules
import numpy as np

# Project Modules
import cde.gmix as gmix

np.set_printoptions(precision=12, suppress=True)

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)-15s [%(name)s]:%(lineno)d %(levelname)s %(message)s'
)


class TestGaussianMixture(unittest.TestCase):
    weights_1 = np.array([[0.15, 0.10, 0.40, 0.05, 0.3]])
    means_1 = np.array([[0, 1, 2, 3, 4]])
    sigmas_1 = np.array([[0.1, 0.5, 2, 1, 3]])

    weights_2 = np.array([
        [0.15, 0.10, 0.40, 0.05, 0.3],
        [0.05, 0.20, 0.10, 0.35, 0.3]
    ])
    means_2 = np.array([
        [+0, +1, 2, 3, 4],
        [-5, -2, 0, 1, 3]
    ])
    sigmas_2 = np.array([
        [0.9, 0.5, 2.0, 1.0, 3.0],
        [0.5, 10.0, 2.0, 5.0, 0.8]
    ])

    def test_single_parameter_pdf(self):
        weights, means, sigmas = self.weights_1, self.means_1, self.sigmas_1

        act_pdf_1_1 = gmix.pdf(1, weights, means, sigmas)
        exp_pdf_1_1 = [0.1770981422]

        act_pdf_1_2 = gmix.pdf(1, weights[0], means[0], sigmas[0])
        exp_pdf_1_2 = [0.1770981422]

        act_pdf_2_1 = gmix.pdf(-5.5, weights, means, sigmas)
        exp_pdf_2_1 = [0.000335616732]

        np.testing.assert_allclose(act_pdf_1_1, exp_pdf_1_1)
        np.testing.assert_allclose(act_pdf_1_2, exp_pdf_1_2)
        np.testing.assert_allclose(act_pdf_2_1, exp_pdf_2_1)

    def test_multi_parameter_pdf(self):
        weights, means, sigmas = self.weights_2, self.means_2, self.sigmas_2

        act_pdf_1 = gmix.pdf(1, weights, means, sigmas)
        exp_pdf_1 = [0.212963552378, 0.059730094961]

        act_pdf_2 = gmix.pdf(-5.5, weights, means, sigmas)
        exp_pdf_2 = [0.000335617249, 0.044152358959]

        np.testing.assert_allclose(act_pdf_1, exp_pdf_1)
        np.testing.assert_allclose(act_pdf_2, exp_pdf_2)

    def test_single_parameter_cdf(self):
        weights, means, sigmas = self.weights_1, self.means_1, self.sigmas_1

        act_cdf_1 = gmix.cdf(1, weights, means, sigmas)
        exp_cdf_1 = [0.3721490983]

        act_cdf_2 = gmix.cdf(-5.5, weights, means, sigmas)
        exp_cdf_2 = [0.000266662349]

        np.testing.assert_allclose(act_cdf_1, exp_cdf_1)
        np.testing.assert_allclose(act_cdf_2, exp_cdf_2)

    def test_multi_parameter_cdf(self):
        weights, means, sigmas = self.weights_2, self.means_2, self.sigmas_2

        act_cdf_1 = gmix.cdf(1, weights, means, sigmas)
        exp_cdf_1 = [0.352160058832, 0.419591430163]

        act_cdf_2 = gmix.cdf(-5.5, weights, means, sigmas)
        exp_cdf_2 = [0.000266662424, 0.11474477839]

        np.testing.assert_allclose(act_cdf_1, exp_cdf_1)
        np.testing.assert_allclose(act_cdf_2, exp_cdf_2)

    def test_single_parameter_ppf(self):
        weights, means, sigmas = self.weights_1, self.means_1, self.sigmas_1

        act_ppf_1 = gmix.ppf(0.3, weights, means, sigmas)
        exp_ppf_1 = [0.550182597797]

        act_ppf_2 = gmix.ppf(0.9, weights, means, sigmas)
        exp_ppf_2 = [5.673110996415]

        np.testing.assert_allclose(act_ppf_1, exp_ppf_1)
        np.testing.assert_allclose(act_ppf_2, exp_ppf_2)

    def test_multi_parameter_ppf(self):
        weights, means, sigmas = self.weights_2, self.means_2, self.sigmas_2

        act_ppf_1 = gmix.ppf(0.3, weights, means, sigmas)
        exp_ppf_1 = [0.753339733752, -1.185816277994]

        act_ppf_2 = gmix.ppf(0.9, weights, means, sigmas)
        exp_ppf_2 = [5.673110996856, 5.916783093835]

        np.testing.assert_allclose(act_ppf_1, exp_ppf_1)
        np.testing.assert_allclose(act_ppf_2, exp_ppf_2)

    def test_normal_derivative_1(self):
        weights, means, sigmas = self.weights_1, self.means_1, self.sigmas_1

        eps = 1e-6

        x = 0.5

        fpe = gmix.pdf(x+eps, weights, means, sigmas)
        fme = gmix.pdf(x-eps, weights, means, sigmas)

        num_der = ((fpe - fme) / (2 * eps))[0]
        ana_der = gmix._gmix_derivative_1(0.5, weights, means, sigmas)

        self.assertAlmostEqual(num_der, ana_der)

    def test_normal_derivative_2(self):
        weights, means, sigmas = self.weights_1, self.means_1, self.sigmas_1

        eps = 1e-6
        x = 0.5

        fpe = gmix._gmix_derivative_1(x + eps, weights, means, sigmas)
        fme = gmix._gmix_derivative_1(x - eps, weights, means, sigmas)

        num_der = ((fpe - fme) / (2 * eps))
        ana_der = gmix._gmix_derivative_2(0.5, weights, means, sigmas)

        self.assertAlmostEqual(num_der, ana_der)

    # @unittest.skip("Temporary")
    def test_mode(self):
        weights, means, sigmas = self.weights_2, self.means_2, self.sigmas_2

        act_modes_newt = gmix.mode(weights, means, sigmas, method='newton')
        exp_modes_newt = [
            [(0.9580152000982137, 0.21324093573952826)],
            [
                (-4.970966372242318, 0.06205794913899075),
                (2.96872434469676, 0.189013117805071)
            ]
        ]

        for act_grp, exp_grp in zip(act_modes_newt, exp_modes_newt):
            for (act_x, act_p), (exp_x, exp_p) in zip(act_grp, exp_grp):
                self.assertAlmostEqual(act_x, exp_x)
                self.assertAlmostEqual(act_p, exp_p)

        act_modes_cnewt = gmix.mode(weights, means, sigmas, method='cnewton')

        for act_grp, exp_grp in zip(act_modes_cnewt, exp_modes_newt):
            for (act_x, act_p), (exp_x, exp_p) in zip(act_grp, exp_grp):
                self.assertAlmostEqual(act_x, exp_x)
                self.assertAlmostEqual(act_p, exp_p)

        act_modes_line = gmix.mode(weights, means, sigmas, method='line', **{'precision': 0.01})
        exp_modes_line = [
            [
                (0.9600000000000012, 0.21324031649238523)
            ],
            [
                (-4.9700000000000175, 0.062057875637029594),
                (2.9699999999998132, 0.18901292890362817)
            ]
        ]

        for act_grp, exp_grp in zip(act_modes_line, exp_modes_line):
            for (act_x, act_p), (exp_x, exp_p) in zip(act_grp, exp_grp):
                self.assertAlmostEqual(act_x, exp_x)
                self.assertAlmostEqual(act_p, exp_p)

    def test_random(self):
        weights, means, sigmas = self.weights_2, self.means_2, self.sigmas_2

        rng = np.random.RandomState(2177)

        rvalues = gmix.random(5, weights, means, sigmas, rng)

        print(rvalues)

    @unittest.skip('Nothing to assert')
    def test_plot(self):
        weights, means, sigmas = self.weights_2, self.means_2, self.sigmas_2

        p = gmix.plot(
            weights,
            means,
            sigmas,
            row_names=['a', 'b'],
            show_mixture=False
        )

        print(p)

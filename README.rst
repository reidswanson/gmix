#################
Gaussian Mixtures
#################

.. image:: docs/source/images/single-density-plot.png
    :width: 100%
    :alt: Single Mixture Density Plot

********
Overview
********
This project has two objectives:

1. Provide a set of functions for working with univariate `Gaussian Mixture distributions <https://brilliant.org/wiki/gaussian-mixture-model/>`_ similar to `dmixnorm <https://rdrr.io/cran/KScorrect/man/dmixnorm.html>`_ of the `KScorrect <https://rdrr.io/cran/KScorrect/>`_ R package.
2. Provide a recipe for implementing `conditional density estimation (CDE) <https://en.wikipedia.org/wiki/Density_estimation>`_ using (Gaussian) kernel methods combined with neural networks.

************
Dependencies
************

* (required) `numpy (1.17) <https://numpy.org/install/>`_
* (required) `scipy (1.3.1) <https://www.scipy.org/install.html>`_
* (needed for plotting) `pandas (0.25.1) <https://pandas.pydata.org/pandas-docs/stable/getting_started/install.html>`_
* (needed for plotting) `plotnine (0.6.0) <https://plotnine.readthedocs.io/en/stable/installation.html>`_
* (needed for CDE and demo) `TensorFlow (1.15) <https://www.tensorflow.org/install/pip>`_
* (needed for CDE and demo) `TensorFlow Probability <https://www.tensorflow.org/probability>`_
* (needed for CDE and demo) `Keras (2.3.1) <https://keras.io/>`_
* (needed to make kernel centers using the Jenks algorithm) `jenkspy (0.1.5) <https://github.com/mthh/jenkspy>`_

A full list of packages, not all of which are required, are given in the `requirements file <https://bitbucket.org/reidswanson/gmix/src/master/requirements.txt>`_.

*******
Install
*******
The project is not currently on PyPI.
The easiest way to use or install the project is to clone it or via pip using its native Git support.
Note you will need a working C++ compiler.

.. code-block:: bash

   pip install git+https://bitbucket.org/reidswanson/gmix.git

*************
Documentation
*************

The full documentation is available on `Read the Docs <https://gmix.readthedocs.io/en/latest/>`_.

*******
License
*******
Any file without a specific copyright notification is released using the Apache v2 License by me (Reid Swanson).
See the `LICENSE <LICENSE>`_ file for more details.
Some code has been copied or adapted from the web (e.g., StackOverflow) that does not have clear license indications.
In those cases the code is flagged with comments and links to where the code was found.

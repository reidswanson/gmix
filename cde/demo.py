# Copyright 2020 Reid Swanson
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Python Modules
import logging
import math
import os
import random
import warnings

from argparse import ArgumentParser
from functools import partial, update_wrapper

# 3rd Party Modules
import keras.backend.tensorflow_backend as tb
import numpy as np
import pandas as pd
import tensorflow as tf

from keras.layers import Input, Dense, concatenate
from keras.models import Model
from keras.optimizers import Adam
from keras.utils import to_categorical
from plotnine import ggplot, aes, facet_wrap, geom_density, geom_bar, scale_x_continuous
from sklearn.model_selection import train_test_split

# Project Modules
import cde.gmix as gmix
import cde.kmn as kmn
import cde.umix as umix

from cde.data import gaussian_mixture_dataset

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)-15s [%(name)s]:%(lineno)d %(levelname)s %(message)s'
)
logging.getLogger('matplotlib').setLevel(logging.WARNING)
log = logging.getLogger(__name__)


def allow_growth():
    config = tf.compat.v1.ConfigProto()
    config.gpu_options.allow_growth = True

    session = tf.compat.v1.Session(config=config)
    tb.set_session(session)


# region Kernel Mixture Network
def build_kmn_model(n_features: int, kernels: np.ndarray, sigma, shared_layer: bool = False):
    log.debug(
        f"Building model with {n_features} features "
        f"and {kernels.shape[0]} kernels."
    )
    n_units = 200
    activation = 'relu'
    input_lyr = Input(shape=(n_features,))

    # Layers for the kernel weights
    w_dense_1 = Dense(n_units, activation=activation)(input_lyr)
    w_softmax = Dense(kernels.shape[0], activation='softmax')(w_dense_1)
    kernel_weights = w_softmax

    # Use a fixed (non trainable sigma)
    if isinstance(sigma, float):
        outputs = kernel_weights
        loss = partial(kmn.loss, kernel_centers=kernels, sigma=sigma)
        loss = update_wrapper(loss, kmn.loss)
    elif sigma is None or sigma == 1:
        # Layers for the kernel bandwidths
        n_out = 1 if sigma == 1 else kernels.shape[0]

        b_input = concatenate([input_lyr, w_dense_1]) if shared_layer else input_lyr
        b_dense_1 = Dense(n_units, activation=activation)(b_input)
        b_dense_2 = Dense(n_out, activation='softplus')(b_dense_1)
        bandwidths = b_dense_2

        loss_kw = {
            'kernel_centers': kernels,
            'sigma': 1 if sigma == 1 else None,
            'penalize_sigmas': 0.01
        }

        outputs = concatenate([kernel_weights, bandwidths])
        loss = partial(kmn.loss, **loss_kw)
        loss = update_wrapper(loss, kmn.loss)
    else:
        raise ValueError(f"Invalid sigma value: {sigma}")

    optimizer = Adam()

    model = Model(inputs=input_lyr, outputs=outputs)
    model.compile(
        optimizer=optimizer,
        loss=loss,
        # metrics=[most_likely_estimate, mean_value_estimate]
    )

    return model
# endregion


# region QuantizedSoftmax
def build_qsm_model(n_classes, n_bins):
    input_lyr = Input(shape=(n_classes, ))

    # The Quantized Softmax Model is straightforward
    w_dense_1 = Dense(50, activation='relu')(input_lyr)
    w_dense_2 = Dense(50, activation='relu')(w_dense_1)
    w_dense_3 = Dense(50, activation='relu')(w_dense_2)
    w_softmax = Dense(n_bins, activation='softmax')(w_dense_3)

    outputs = w_softmax

    model = Model(inputs=input_lyr, outputs=outputs)
    model.compile(
        optimizer='adam',
        loss='categorical_crossentropy',
        metrics=['accuracy']
    )

    return model
# endregion


def quantized_softmax(args):
    dataset_size = args.dataset_size
    n_classes = args.n_classes
    n_mixtures = args.n_mixtures
    n_bins = args.n_bins
    random_seed = args.random_seed

    if random_seed is not None:
        random.seed(random_seed)

    # rng = np.random.RandomState(random_seed)

    # Build the model
    model = build_qsm_model(n_classes, n_bins)

    # Make a dataset (mu, sigma, and alpha could be used to recreate the true
    # distributions analytically if desired).
    x, y, mu, sigma, alphas = gaussian_mixture_dataset(dataset_size, n_classes, n_mixtures)

    # Split the dataset into a training and testing component.
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25)

    # Convert the x data into 1-hot encoded vectors.
    x_train, x_test = to_categorical(x_train, n_classes), to_categorical(x_test, n_classes)

    # Get the bins
    bins = umix.make_bins(y_train, n_bins)

    # Digitize the y values (convert the raw y value into its bucketed value)
    y_train_bkt = umix.get_bin(y_train, bins)

    # Build and train the model
    model.fit(
        x_train,
        y_train_bkt,
        epochs=2,
        batch_size=256,
        validation_split=0.1,
        shuffle=True,
        verbose=0
    )

    # Get the predictions
    y_pred = model.predict(x_test, batch_size=1024)

    # Get the centers
    centers = umix.bin_centers(bins)

    # Make some plots
    y_min, y_max = min(y), max(y)
    y_pred_df = pd.DataFrame(y_pred)
    y_pred_df['x'] = np.argmax(x_test, axis=1)
    y_pred_df = y_pred_df.groupby('x').mean()
    y_pred_df = y_pred_df.unstack().reset_index()
    y_pred_df['level_0'] = y_pred_df['level_0'].apply(lambda i: centers[i])
    y_pred_df.columns = ['bin', 'x', 'p']

    p1 = (
        ggplot(y_pred_df, aes('bin', 'p'))
        + geom_bar(stat='identity')
        + scale_x_continuous(limits=(y_min, y_max))
        + facet_wrap('~x', nrow=math.sqrt(n_classes), scales='fixed')
    )

    df = pd.DataFrame({'x': x, 'y': y})

    p2 = (
        ggplot(df, aes('y'))
        + geom_density()
        + scale_x_continuous(limits=(y_min, y_max))
        + facet_wrap('~x', nrow=math.sqrt(n_classes), scales='fixed')
    )

    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        p1.save(os.path.join(args.output_dir, 'qsm-pred.pdf'), limitsize=False)
        p2.save(os.path.join(args.output_dir, 'qsm-true.pdf'), limitsize=False)


def kernel_mixture(args):
    dataset_size = args.dataset_size
    n_classes = args.n_classes
    n_mixtures = args.n_mixtures
    kernel_method = args.kernel_method
    n_kernels = args.n_kernels
    n_epochs = args.n_epochs
    random_seed = args.random_seed
    output_dir = args.output_dir

    if random_seed is not None:
        random.seed(random_seed)

    rng = np.random.RandomState(random_seed)

    allow_growth()

    # Make a dataset (mu, sigma, and alpha could be used to recreate the true
    # distributions analytically if desired).
    x, y, mu, sigma, alphas = gaussian_mixture_dataset(dataset_size, n_classes, n_mixtures, rng)

    x_plot_min, x_plot_max = np.min(mu) - 3 * np.max(sigma), np.max(mu) + 3 * np.max(sigma)
    p = gmix.plot(
        alphas,
        mu,
        sigma,
        x_min=x_plot_min,
        x_max=x_plot_max,
        n_points=1000,
        show_mixture=True,
        show_mean=True,
        show_median=True,
        show_mode=True,
        percent_interval=None,
        ncol=3,
        figure_size=(32, 23),
    )

    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        p.save(os.path.join(output_dir, 'kmn-true-dist.pdf'), limitsize=False)

    # Split the dataset into a training and testing component.
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25)

    # Convert the x data into 1-hot encoded vectors.
    x_train, x_test = to_categorical(x_train, n_classes), to_categorical(x_test, n_classes)

    # Get the kernel centers
    centers = gmix.make_centers(
        y_train,
        n_kernels,
        method=kernel_method
    )

    # Build the model
    sigma_model = None
    model = build_kmn_model(n_classes, centers, sigma=sigma_model, shared_layer=True)

    # Reshape the target data to match the shape of the predictions
    y_train, y_test = kmn.reshape_targets(y_train, centers), kmn.reshape_targets(y_test, centers)

    # Build and train the model
    model.fit(
        x_train,
        y_train,
        epochs=n_epochs,
        batch_size=256,
        validation_split=0.1,
        shuffle=True,
        verbose=0
    )

    # Get the predictions
    y_pred = model.predict(x_test, batch_size=1024)

    # Some plots
    groups = np.argmax(x_test, axis=1)
    mu_hat = np.tile(centers, (args.n_classes, 1))
    alpha_hat = y_pred[:, :n_kernels]

    if sigma_model is None:
        sigma_hat = y_pred[:, n_kernels:]
    elif isinstance(sigma_model, int) and sigma_model == 1:
        sigma_hat = np.repeat(y_pred[:, n_kernels], n_kernels).reshape((len(y_pred), -1))
    else:
        # noinspection PyTypeChecker
        sigma_hat = np.repeat(sigma_model, len(y_pred) * n_kernels).reshape((len(y_pred), -1))

    def extract_values(values, prefix):
        df = pd.DataFrame(values, columns=[f'{prefix}{i}' for i in range(n_kernels)])
        df['group'] = groups
        df = df.drop_duplicates(subset='group').sort_values('group')

        return df.iloc[:, :-1].values

    alpha_hat = extract_values(alpha_hat, 'a')
    sigma_hat = extract_values(sigma_hat, 's')

    alpha_hat = np.nan_to_num(alpha_hat)
    sigma_hat = np.nan_to_num(sigma_hat)
    sigma_hat[sigma_hat < 1e-7] = 99

    p = gmix.plot(
        alpha_hat,
        mu_hat,
        sigma_hat,
        x_min=x_plot_min,
        x_max=x_plot_max,
        n_points=1500,
        show_mixture=True,
        show_mean=True,
        show_median=True,
        show_mode=True,
        percent_interval=None,
        ncol=3,
        figure_size=(30, 23),
    )

    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        p.save(os.path.join(output_dir, 'kmn-pred-dist.pdf'), limitsize=False)


def make_command_line_options():
    cli = ArgumentParser()
    subparsers = cli.add_subparsers(help='')

    # region Common Arguments
    cli.add_argument(
        '--output-dir',
        required=True,
        help="Directory where graphs will be output."
    )

    cli.add_argument(
        '--dataset-size',
        required=False,
        type=int,
        default=50000,
        help="The number of synthetic data points to generate."
    )

    cli.add_argument(
        '--n-classes',
        required=False,
        type=int,
        default=9,
        help="The number of unique mixture distributions to generate from."
    )

    cli.add_argument(
        '--n-mixtures',
        required=False,
        type=int,
        default=4,
        help=(
            "The number of Normal distributions that each mixture distribution "
            "will be composed of."
        )
    )

    cli.add_argument(
        '--n-epochs',
        required=False,
        type=int,
        default=10,
        help="The number of epochs to train for."
    )

    cli.add_argument(
        '--random-seed',
        required=False,
        type=int,
        default=None,
        help="A random seed for reproducibility."
    )
    # endregion

    # region QSM Arguments
    qsm_cli = subparsers.add_parser('qsm', help='Demo the Quantized Softmax model.')
    qsm_cli.set_defaults(func=quantized_softmax)

    qsm_cli.add_argument(
        '--n-bins',
        required=False,
        type=int,
        default=40,
        help="The number of histogram bins."
    )

    qsm_cli.add_argument(
        '--random-seed',
        required=False,
        type=int,
        default=None,
        help="A random seed for reproducibility."
    )
    # endregion

    # region KMN Arguments
    kmn_cli = subparsers.add_parser('kmn', help='Demo the Kernel Mixture Network model.')
    kmn_cli.set_defaults(func=kernel_mixture)

    kmn_cli.add_argument(
        '--n-kernels',
        required=False,
        type=int,
        default=50,
        help="The number of Gaussian kernels in the model will use."
    )

    kmn_cli.add_argument(
        '--kernel-method',
        required=False,
        type=str,
        choices=('jenks', 'uniform'),
        default='uniform',
        help="How to determine the mean values (centers) for each kernel."
    )
    # endregion

    return cli


if __name__ == '__main__':
    clo = make_command_line_options()

    parsed_args = clo.parse_args()
    parsed_args.func(parsed_args)

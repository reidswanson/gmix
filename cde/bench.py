# Copyright 2020 Reid Swanson
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Python Modules
import logging
import time

# 3rd Party Modules
import numpy as np
import scipy.stats

# Project Modules
import cde.gmix as gmix
import cgmix


logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)-15s [%(name)s]:%(lineno)d %(levelname)s %(message)s'
)
logging.getLogger('matplotlib').setLevel(logging.WARNING)
log = logging.getLogger(__name__)


if __name__ == '__main__':
    dim = 150
    size = 20

    n_runs = 15

    p_results = []
    c_results = []

    for _ in range(n_runs):
        weights = np.random.dirichlet([1 for i in range(dim)], size)
        means = np.random.uniform(1, 10, (size, dim))
        sigmas = np.random.uniform(0.5, 3, (size, dim))

        start = time.clock()
        gmix_modes = gmix.mode(weights, means, sigmas, method="newton")
        stop = time.clock()
        p_results.append((stop - start))

        start = time.clock()
        cgmix_modes = gmix.mode(weights, means, sigmas, method="cnewton")
        stop = time.clock()
        c_results.append((stop - start))

    log.info(f"Newton's method in python: {scipy.stats.describe(np.array(p_results))}")
    log.info(f"Newton's method in c++: {scipy.stats.describe(np.array(c_results))}")

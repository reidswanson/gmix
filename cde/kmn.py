# Copyright 2020 Reid Swanson
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

r"""
This module provides methods to help implement `kernel density
estimation <https://en.wikipedia.org/wiki/Kernel_density_estimation>`_ using
neural networks following the work of
`Ambrogioni et al. <https://arxiv.org/abs/1705.07111>`_ [1]_.

A Kernel Mixture Network is a neural network topology and associated
loss function that allows learning the weights and bandwidths for each
kernel (centroid/mean) in a mixture model. In general the architecture
can support any kernel function, but this module only provides support
for Normal distributions.

Given a set of input data a collection of kernel centers can be obtained
using the :func:`make_centers` method.
Construct a new object from the given Keras :code:`model` and set of
:code:`kernel_centers`.

The following description provides a recipe for creating models that are
compatible with the functions in this module.
A model is expected to have 1 of 3 topologies. The input and hidden layers
are basically unconstrained except as specified below.

a. **No bandwidths** - This topology only learns the weights for
   each kernel. The only requirement of this model is that the
   final output layer must have exactly \|K\| (number of kernel centers)
   units and use a :code:`softmax` activation.
b. **Single bandwidth** - This topology learns the weights for each
   kernel and a single bandwidth that is used for every kernel.
   This model expects a final layer that has exactly \|K\|+1 units. The
   first \|K\| should be the result of a softmax layer at the
   :code:`n-1` layer (for representing the kernel weights). The final
   unit should be the result of layer that uses an activation function
   that produces non-negative values. :code:`softplus` seems to work
   well (:code:`ReLU` does not).
c. **With bandwidths** - In addition to the weights this topology
   learns a separate bandwidth for each kernel. This model expects
   a final layer that has exactly 2 &middot; \|K\| units. The first
   \|K\| must come from a layer using a :code:`softmax` activation
   and the 2nd \|K\| must come from a layer using an activation
   function that produces non-negative values.

.. table::
    :align: center
    :widths: 30 30 30

    +-----------------------+--------------------------+-----------------------------+
    | |kmn_model_1|         | |kmn_model_2|            | |kmn_model_3|               |
    +=======================+==========================+=============================+
    | **(a)** No bandwidths | **(b)** Single bandwidth | **(c)** Multiple bandwidths |
    +-----------------------+--------------------------+-----------------------------+

See the
`demo <https://bitbucket.org/reidswanson/kernel-mixture-networks/src/ed2074d7991a560dfe0d6bf126d1854d27fa2f62/cde/demo.py#lines-50>`_
program for examples of how to build each type of model. It is recommended to use
topology **(c)**.

.. |kmn_model_1| image:: _static/kmn_without_bandwidths.png
    :scale: 26%

.. |kmn_model_2| image:: _static/kmn_with_single_bandwidth.png
    :scale: 25%

.. |kmn_model_3| image:: _static/kmn_with_bandwidths.png
    :scale: 25%

.. [1] Ambrogioni, L., Güçlü, U., van Gerven, M. A. J., & Maris, E. (2017).
   The Kernel Mixture Network: A Nonparametric Method for Conditional Density
   Estimation of Continuous Random Variables. ArXiv:1705.07111 [Stat].
"""
# Python Modules
import logging

from typing import Union

# 3rd Party Modules
import keras.backend as be
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp

from keras.losses import mean_squared_error

log = logging.getLogger(__name__)


# Note these resources were helpful for me:
# https://en.wikipedia.org/wiki/Mixture_distribution
# https://stats.stackexchange.com/questions/177049/quantile-function-for-a-mixture-model
# https://stats.stackexchange.com/questions/14481/quantiles-from-the-combination-of-normal-distributions
# http://www.awebb.info/blog/quantiles_of
# https://rdrr.io/cran/KScorrect/man/dmixnorm.html
# https://pdfs.semanticscholar.org/3bdc/57ab702bfdd2c936599a535b75c0675b56f9.pdf


def reshape_targets(y_data: np.ndarray, centers: Union[list, np.ndarray], sigma=None):
    """
    Given a set of target data that would be appropriate for a regression
    task, this method will reshape the data so that it is appropriate
    for a kernel mixture network using the :func:`kmn_loss` function.

    :param y_data: Target values of shape (batch_size, 1)
    :param centers: The center value for each kernel (i.e., the means of each
                    Gaussian in the mixture).
    :param sigma: :code:`sigma` can either be :code:`None`, the integer
                  :code:`1`, or a single floating point number. These
                  correspond to models **(c)**, **(b)**, and **(a)**
                  as described in :meth:`~__init__` respectively.

    :return: The reshaped target data.
    """
    y_data = np.repeat(y_data, len(centers)).reshape(len(y_data), -1)

    if sigma is None:
        return np.tile(y_data, 2)
    elif isinstance(sigma, int) and sigma == 1:
        return np.append(y_data, np.zeros((len(y_data), 1)), 1)
    else:
        return y_data


def loss(y_true, y_pred, kernel_centers, sigma=None, penalize_sigmas=0):
    """
    Calculates the negative log likelihood of the kernel mixture.
    Keras loss functions are required to have only 2 parameters. However,
    the function requires the kernel centers, which are passed as a 3rd
    parameter. To make use of this function as a Keras loss function, you
    can use :code:`functools.partial` to fill in the kernel centers and create
    a partial function with the correct number of parameters.

    .. code-block:: python

        from functools import partial

        y_pred = ...
        y_true = ...
        kernel_centers = np.array([0, 2, 4, 6])
        my_loss = partial(base_loss, kernel_centers=kernel_centers)

    :param y_true: The true target values you are trying to estimate the
                   density of. This array needs to be the same dimensions
                   as :code:`y_pred` and have values repeated in a specific
                   format for the loss function to work. The utility
                   function :meth:`~KernelMixtureNetwork.reshape_targets`
                   can be used to transform a dataset you would use for a
                   standard regression problem (e.g. shape=(_, 1)) into
                   the one required for the loss function to work.
    :param y_pred: The estimated kernel parameters from the network.
    :param kernel_centers: The kernel centers used by the KernelMixtureNetwork.
    :param sigma: sigma can take one of three values:

                  1. :code:`None` -- This indicates to the loss function to
                     expect topology
                     :meth:`(c) <KernelMixtureNetwork.__init__>`
                  2. An integer :code:`1` -- This indicates to the loss to
                     expect topology
                     :meth:`(b) <KernelMixtureNetwork.__init__>`
                  3. A single floating point number -- This indicates to the
                     loss function to expect topology
                     :meth:`(a) <KernelMixtureNetwork.__init__>`
    :param penalize_sigmas: Add a term that penalizes sigmas that are less
                            than this threshold.

    :return: The negative log likelihood for each data point.
    """
    # Part of y_true is duplicated in order to make its shape match y_pred.
    # We only need the unique part of it, so it is extracted here.
    n_kernels = len(kernel_centers)
    y_true = y_true[:, :n_kernels]

    # Get the kernel weights and bandwidths from the predictions.
    weights = y_pred[:, :n_kernels]

    if sigma is None:
        sigmas = y_pred[:, n_kernels:] + be.epsilon()
    elif isinstance(sigma, int) and sigma == 1:
        sigmas = y_pred[:, n_kernels] + be.epsilon()
    else:
        sigmas = sigma

    dist = tfp.distributions.Normal(tf.cast(kernel_centers, tf.float32), sigmas)
    probs = dist.prob(y_true)
    weighted_probs = be.sum(weights * probs, axis=1)
    result = -be.log(weighted_probs)

    if penalize_sigmas > 0:
        mask = be.cast(be.less(sigmas, penalize_sigmas), 'float32')
        nlp = -be.log(sigmas)
        penalization = be.sum(mask * nlp)

        result += penalization

    return result


def mean_mean_squared_error(y_true, y_pred, kernel_centers):
    """
    Calculate the mean squared error based on the mean value of the resulting
    kernel density estimate. Since the returned value of the
    :func:`loss function <base_loss>` is not easy to interpret this method
    can be used as a metric to get a better understanding of how the training
    is going.

    :param y_true: The true values as created by
                   :meth:`KernelMixtureNetwork.reshape_targets`.
    :type y_true: Tensor
    :param y_pred: The predicted parameters of the kernel density estimate.
    :type y_pred: Tensor
    :param kernel_centers: The kernel centers (as created by
                           :meth:`KernelMixtureNetwork.make_kernel_centers`).
    :type kernel_centers: np.ndarray
    :return: The mean squared error using the mean value of the kernel mixture.
    :rtype: Tensor
    """
    n_kernels = len(kernel_centers)
    y_true = y_true[:, 0]

    # Get the kernel weights and bandwidths from the predictions.
    weights = y_pred[:, :n_kernels]

    means = be.sum(weights * kernel_centers, axis=1)

    return mean_squared_error(y_true, means)

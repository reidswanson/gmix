# Copyright 2020 Reid Swanson
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
This module provides methods for a mixture of uniform distributions (i.e.,
a histogram).
"""
# Python Modules

# 3rd Party Modules
import numpy as np

from keras.utils import to_categorical

# Project Modules


def make_bins(data: np.ndarray, n_bins: int):
    """
    Create an array of equally sized bins that values in :code:`data` can
    belong to. This is done by finding the minimum and maximum value in
    :code:`data` and then splitting them into :code:`n_bins` equal parts
    (using :code:`np.linspace`).

    :param data: This is expected to be a 1-D array of continuous values.
    :param n_bins: The number of bins to create.
    :return: An :code:`np.ndarray` with :code:`n_bins` elements equally
             spaced values ranging from :code:`min(data)` to
             :code:`max(data)`
    """
    y_min, y_max = min(data), max(data)

    return np.linspace(y_min, y_max, num=n_bins)


def bin_centers(bins: np.ndarray):
    bin_width = bins[1] - bins[0]
    return bins - (bin_width / 2)


def get_bin(y_data: np.ndarray, bins: np.ndarray):
    """
    Finds the bin that each instance in :code:`y_data` (i.e., the raw target
    values) belong to and then converts it into a 1-hot encoding.

    Given a set of bins, np.digitize will assign each value in y_data
    to the index of which bin it belongs. You can think of the values
    in the bins as the right edge of each interval. For example,
    given a bins array :code:`[0, 2, 4, 6]`::

          -inf <= x < 0     would be in bin 0
             0 <= x < 2     would be in bin 1
             2 <= x < 4     would be in bin 2
             4 <= x < 6     would be in bin 3
             6 <= x < inf   would be in bin 4

    :param y_data: This is expected to be a 1-D array of continuous values.
    :param bins: The histogram bins (i.e., made by :func:`make_bins`.
    :return: A 2-D array where each row corresponds to an input value
             and the columns represent the bin the value belongs to using a 1-hot
             encoding.
    """
    n_bins = len(bins)

    # Notice that values greater than or equal to the max return an index
    # greater than the number of bins. So any value larger than this is
    # mapped back to the last bin (using np.minimum)
    digitized = np.minimum(np.digitize(y_data, bins), n_bins - 1)

    # The result of the digitization is then converted to a 1-hot encoded
    # 2-D array
    # return to_categorical(digitized, n_bins)
    return np.eye(n_bins)[digitized]


def pdf(y: float, bins: np.ndarray, weights: np.ndarray):
    bucket = np.minimum(np.digitize(y, bins), len(bins) - 1)

    return weights[:, bucket]


def cdf(y: float, bins: np.ndarray, weights: np.ndarray):
    bucket = np.minimum(np.digitize(y, bins), len(bins) - 1)

    return np.sum(weights[:, :bucket+1], axis=1).ravel()


def ppf(p: float, bins: np.ndarray, weights: np.ndarray, centers: np.ndarray = None):
    if not (0 < p < 1):
        raise ValueError(f"p must be in the range 0 < p < 1: p = {p}")

    if not centers:
        centers = bin_centers(bins)

    cdf_ = np.array([cdf(y, bins, weights) for y in centers]).transpose()
    result = np.array([centers[np.searchsorted(row, p)] for row in cdf_])

    return result


def mean(bins: np.ndarray, weights: np.ndarray, **kwargs):
    return np.sum(weights * bins, axis=1)


def mode(bins: np.ndarray, weights: np.ndarray, **kwargs):
    return bins[np.argmax(weights, axis=1)]

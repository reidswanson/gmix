# Copyright 2020 Reid Swanson
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Create synthetic data to test the models on.
"""
# Python Modules
import logging

# 3rd Party Modules
import numpy as np

# Project Modules

log = logging.getLogger(__name__)


def gaussian_mixture_dataset(
        n_samples: int = 50000,
        n_buckets: int = 9,
        n_mix: int = 2,
        rng: np.random.RandomState = None
):
    """
    Create a synthetic dataset where a datapoint is drawn from one of
    :code:`n_buckets` distributions. Each distribution is a mixture of
    Gaussians with its own set of parameters.

    :param n_samples: The total number of samples to draw.
    :type n_samples: int
    :param n_buckets: The number of distinct distributions to draw from.
    :type n_buckets: int
    :param n_mix: The number of Gaussians to mix for each distribution.
    :type n_mix: int
    :return: An 1-D array of data points each drawn from 1 of the
             :code:`n_buckets` distributions.
    :rtype: np.ndarray
    """
    rng = rng or np.random.RandomState()

    def rand(*args):
        try:
            return rng.rand(*args).astype('float32')
        except AttributeError:
            return rng.rand(*args)

    def randn(*args):
        try:
            return rng.randn(*args).astype('float32')
        except AttributeError:
            return rng.randn(*args)

    log.debug(
        f"Creating dataset with {n_samples} samples, {n_buckets} buckets, and {n_mix} mixtures"
    )

    x = rng.randint(0, n_buckets, n_samples)
    y = np.zeros((n_samples, ), dtype=np.float32)

    # Create parameters for the Gaussian mixtures (for each x value)
    mu = rand(n_buckets, n_mix) * 100
    sigma = rand(n_buckets, n_mix) * 9 + 1
    alphas = rng.dirichlet(np.ones(n_mix), n_buckets)

    for (index,), x_value in np.ndenumerate(x):
        rnd = randn(n_mix)
        gaussians = sigma[x_value, :] * rnd + mu[x_value, :]
        y[index] = rng.choice(gaussians, p=alphas[x_value])

    return x, y, mu, sigma, alphas


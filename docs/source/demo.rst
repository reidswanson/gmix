.. _sec_demo:

####
Demo
####

The :mod:`cde.demo` module contains `a script <https://bitbucket.org/reidswanson/gmix/src/master/cde/demo.py>`_ that demonstrates how to build a network as described in :ref:`the previous section <sec_cde>`.
The program will create an artificial dataset that will be used for training and testing the model.
It will create 9 different distributions each of which is a mixture of 6 Normal distributions.
The :math:`x` value for a data point is a one-hot encoded variable (0-8) and the :math:`y` value is a random value drawn from the corresponding mixture distribution.
The full set of command line options is :ref:`described below <demo_sec_clo>`.

.. code-block:: bash

    >python -m cde.demo         \
        --output-dir /tmp       \
        --random-seed 2         \
        --dataset-size 50000    \
        --n-epochs 200          \
        --n-classes 9           \
        --n-mixtures 6          \
        kmn                     \
        --kernel-method uniform \
        --n-kernels 20

After running the command the program will output two files in the ``tmp`` directory.
The first is a plot of the true densities of each of the mixture distributions, shown in :numref:`demo_img_true_dist`.

.. _demo_img_true_dist:
.. figure:: images/kmn-true-dist.svg
    :width: 100%
    :figclass: align-center

    True mixture distributions.

The second is a plot of the predicted densities for each class after 200 epochs, shown in :numref:`demo_img_pred_dist`.

.. _demo_img_pred_dist:
.. figure:: images/kmn-pred-dist.svg
    :width: 100%
    :figclass: align-center

    Predicted mixture distributions.

.. _demo_sec_clo:

********************
Command Line Options
********************
Command line usage for the demo script.

.. argparse::
    :ref: cde.demo.make_command_line_options
    :prog: cde.demo.py

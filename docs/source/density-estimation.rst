.. _sec_cde:

##############################
Conditional Density Estimation
##############################
Conditional density estimation is similar to `kernel density estimation <https://en.wikipedia.org/wiki/Kernel_density_estimation>`_.
In kernel density estimation we are given some data :math:`X` and we would like to estimate the density of that data with a function :math:`f(X)`.
In conditional density estimation we are also given some data :math:`X`, but we would like to estimate the density of some unknown :math:`y` given x with a function :math:`f(y|X)`.
This project implements conditional density estimation using neural networks as described by Ambrogioni et al. [AGGM17]_

The density estimate in this model is given by:

.. math::

    p(y|X) = \sum_{i}^{K} \phi_{i} \mathcal{N}(y|\mu_{i}, \sigma^{2}, X)

Where :math:`K` is the number of Normal distributions in the mixture.
:math:`\phi_{i}` is the weight for the :math:`i` th distribution.
:math:`\mu_{i}` is the mean value for the :math:`i` th distribution.
And :math:`\sigma^{2}` is the variance for the :math:`i` th distribution

:math:`K` and :math:`\mu` are hyperparameters of the model and must be given in advance.
The project provides two methods for generating the kernel centers (:math:`\mu`) given some value of :math:`K` using the function :func:`~cde.gmix.make_centers`.
The first methods splits the target region into uniform intervals and uses the split points as the mean values (i.e., kernel centers).
The alternate (and default) method uses the `Jenks algorithm <https://en.wikipedia.org/wiki/Jenks_natural_breaks_optimization>`_ to find a more optimal partition of the data.

The weights and variances will be learned by the network using the log loss:

.. math::

    \mathcal{L}(y|X) = -\log(p(y|X))

This is implemented in the :func:`~cde.kmn.loss` function.

********************
Network Architecture
********************
There are many ways a network could be constructed given this formulation of the problem, but any architecture must have the following properties.
First, it must take an arbitrary vector of inputs (as any network must).
Second, the output of the network must have exactly :math:`2 \cdot K` elements.
The first :math:`K` represent the weights for each kernel and the second :math:`K` represent the variance for each kernel.

.. _de_img_kmn_architecture:
.. figure:: _static/kmn_with_bandwidths.svg
    :width: 25%
    :figclass: align-center

    Suggested model architecture.

For reasons explained in [NiWe95]_, I suggest an architecture similar to the one given :numref:`de_img_kmn_architecture`.
The input vector is shown at the bottom in yellow.
The hidden layers are then split into two paths.
On the left, in orange, are the layers for learning the kernel weights.
The network can be arbitrarily deep, but the final layer must use a `softmax` activation to ensure that the weights sum to 1.

The right side of the network, in blue, are the layers for learning the variance values.
The first hidden layer uses both the input data and the first hidden layer from the weights side.
This provides some additional flexibility in learning the variances.
This side can also be arbitrarily deep, but the final layer must use an activation that guarantees values greater than 0.
There are several possibilities, but a `softplus` activation seems to work well in practice.

The output of these two sides are then concatenated into a final output layer that can be input to the :func:`~cde.kmn.loss` function.

For a complete example continue to the :ref:`demo application <sec_demo>`.

***************************
Alternative Implementations
***************************
An alternate implementation is described in this `blog post <https://janvdvegt.github.io/2017/06/07/Kernel-Mixture-Networks.html>`_.
This implementation seemed overly complex for my purposes and relies on the Bayesian neural network toolkit `Edward <http://edwardlib.org/tutorials/bayesian-neural-network>`_, which led me develop this implementation using standard TensorFlow and Keras functionality.


.. rubric:: References

.. [AGGM17] Luca Ambrogioni, Umut Güçlü, Marcel A. J. van Gerven, Eric Maris. 2017. The Kernel Mixture Network: A Nonparametric Method for Conditional Density Estimation of Continuous Random Variables. arXiv:1705.07111. Retrieved from `https://arxiv.org/abs/1705.07111 <https://arxiv.org/abs/1705.07111>`_
.. [NiWe95] David A. Nix and Andreas S. Weigend. 1994. `Learning Local Error Bars for Nonlinear Regression <https://papers.nips.cc/paper/896-learning-local-error-bars-for-nonlinear-regression.pdf>`_. In pages 489–496. January.

.. Kernel Mixture Networks documentation master file, created by
   sphinx-quickstart on Fri Feb 15 09:34:52 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#################
Gaussian Mixtures
#################

********
Overview
********
This project has two objectives:

1. Provide a set of functions for working with univariate `Gaussian Mixture distributions <https://brilliant.org/wiki/gaussian-mixture-model/>`_ similar to `dmixnorm <https://rdrr.io/cran/KScorrect/man/dmixnorm.html>`_ of the `KScorrect <https://rdrr.io/cran/KScorrect/>`_ R package.
2. Provide a recipe for implementing `conditional density estimation (CDE) <https://en.wikipedia.org/wiki/Density_estimation>`_ using (Gaussian) kernel methods combined with neural networks.

************
Dependencies
************

* (required) `numpy (1.17) <https://numpy.org/install/>`_
* (required) `scipy (1.3.1) <https://www.scipy.org/install.html>`_
* (needed for plotting) `pandas (0.25.1) <https://pandas.pydata.org/pandas-docs/stable/getting_started/install.html>`_
* (needed for plotting) `plotnine (0.6.0) <https://plotnine.readthedocs.io/en/stable/installation.html>`_
* (needed for CDE and demo) `TensorFlow (1.15) <https://www.tensorflow.org/install/pip>`_
* (needed for CDE and demo) `TensorFlow Probability <https://www.tensorflow.org/probability>`_
* (needed for CDE and demo) `Keras (2.3.1) <https://keras.io/>`_
* (needed to make kernel centers using the Jenks algorithm) `jenkspy (0.1.5) <https://github.com/mthh/jenkspy>`_

A full list of packages, not all of which are required, are given in the `requirements file <https://bitbucket.org/reidswanson/gmix/src/master/requirements.txt>`_.

*******
Install
*******
The project is not currently on PyPI.
The easiest way to use or install the project is to clone it or via pip using its native Git support.
Note you will need a working C++ compiler.

.. code-block:: bash

   pip install git+https://bitbucket.org/reidswanson/gmix.git


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   distribution-functions
   density-estimation
   demo

.. toctree::
   :maxdepth: 2
   :caption: Module Documentation:

   modules

******************
Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

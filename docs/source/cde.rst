cde package
===========

Submodules
----------

.. toctree::

   cde.data
   cde.demo
   cde.gmix
   cde.kmn
   cde.umix

Module contents
---------------

.. automodule:: cde
    :members:
    :undoc-members:
    :show-inheritance:

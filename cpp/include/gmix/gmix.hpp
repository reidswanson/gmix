#pragma once

/*****************************************************************************
 * Copyright 2020 Reid Swanson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*****************************************************************************/

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <stdexcept>
#include <tuple>
#include <vector>

#include <boost/math/special_functions/relative_difference.hpp>
#include <boost/math/distributions/normal.hpp>

using boost::math::relative_difference;
using boost::math::normal_distribution;

namespace gmix {
    template<typename T>
    T first_derivative(
        const T value,
        const std::vector<T> &weights,
        const std::vector<T> &means,
        const std::vector<T> &sigmas
    );

    template<typename T>
    T second_derivative(
        const T value,
        const std::vector<T> &weights,
        const std::vector<T> &means,
        const std::vector<T> &sigmas
    );

    // T should either be float or double
    template <typename T>
    T pdf(
        const T y,
        const std::vector<T> &weights,
        const std::vector<T> &means,
        const std::vector<T> &sigmas
    ) {
        const size_t n_weights = weights.size();
        const size_t n_means = means.size();
        const size_t n_sigmas = sigmas.size();

        if (!(n_weights == n_means && n_weights == n_sigmas)) {
            throw std::invalid_argument(
                "The length of `weights`, `means`, and `sigmas` must all be the same"
            );
        }

        T result = 0.0;
        for (size_t i = 0; i < n_weights; ++i) {
            const T weight = weights[i], mean = means[i], sigma = sigmas[i];
            const auto norm = normal_distribution<T>(mean, sigma);

            result += weight * boost::math::pdf(norm, y);
        }

        return result;
    }

    template <typename T>
    std::vector<std::tuple<T, T>> find_modes_newton(
        const std::vector<T> &weights,
        const std::vector<T> &means,
        const std::vector<T> &sigmas,
        const uint16_t max_itr = 1000,
        const T min_diff = 1e-4,
        const T min_grad = 1e-9
    ) {
        typedef std::vector<std::tuple<T, T>> TupleVector;

        const size_t n_weights = weights.size();
        const size_t n_means = means.size();
        const size_t n_sigmas = sigmas.size();

        if (!(n_weights == n_means && n_weights == n_sigmas)) {
            throw std::invalid_argument(
                "The length of `weights`, `means`, and `sigmas` must all be the same"
            );
        }

        // There appears to be very little benefit to multi-threading this and
        // the overhead could actually make things work.
        // The first pair item holds the mode and the second value holds the
        // pdf of the mode.
        std::vector<std::tuple<T, T>> modes;
        for (size_t mean_idx = 0; mean_idx < n_means; ++mean_idx) {
            uint16_t itr = 0;
            T x_new = means[mean_idx];
            T x_old = 0;

            while (true) {
                const T g1 = first_derivative(x_new, weights, means, sigmas);
                const T g2 = second_derivative(x_new, weights, means, sigmas);

                if (relative_difference(0, g2) < min_grad) { break; }

                x_old = x_new;
                x_new = x_old - (g1 / g2);

                if (itr >= max_itr || std::abs(g1) < min_grad) { break; }

                ++itr;
            }

            // Only consider maxima
            if (second_derivative(x_new, weights, means, sigmas) < 0) {
                // Don't add the mode if we've already found it from a different starting point.
                bool is_close = std::any_of(
                    modes.begin(),
                    modes.end(),
                    [&](std::tuple<T, T> t){
                        auto [mode, p] = t;
                        return std::abs(mode - x_new) < min_diff;
                    }
                );

                modes.push_back(std::make_tuple(x_new, pdf(x_new, weights, means, sigmas)));
            }
        }

        std::sort(modes.begin(), modes.end());
        auto uniq_itr = std::unique(
            modes.begin(),
            modes.end(),
            [&](std::tuple<T, T> left, std::tuple<T, T> right) {
                return std::abs(std::get<0>(left) - std::get<0>(right)) < min_diff;
            }
        );

        modes.resize(std::distance(modes.begin(), uniq_itr));

        return modes;
    }

    template<typename T>
    T first_derivative(
        const T value,
        const std::vector<T> &weights,
        const std::vector<T> &means,
        const std::vector<T> &sigmas
    ) {
        using boost::math::normal_distribution;

        // Assume the weights, means, and sigmas are all the same size.
        size_t n_weights = weights.size();
        T result = 0;
        for (size_t i = 0; i < n_weights; ++i) {
            const auto w = weights[i], mu = means[i], sigma = sigmas[i];

            const auto norm = normal_distribution<T>(mu, sigma);
            result += w * -(value - mu) * boost::math::pdf(norm, value) / (sigma * sigma);
        }

        return result;
    }

    template<typename T>
    T second_derivative(
        const T value,
        const std::vector<T> &weights,
        const std::vector<T> &means,
        const std::vector<T> &sigmas
    ) {
        // Assume the weights, means, and sigmas are all the same size.
        size_t n_weights = weights.size();
        T result = 0;
        for (size_t i = 0; i < n_weights; ++i) {
            const auto w = weights[i], mu = means[i], sigma = sigmas[i];
            const auto norm = normal_distribution<T>(mu, sigma);
            const auto sigma2 = sigma * sigma;
            const auto sigma4 = sigma2 * sigma2;
            const auto diff2 = (value - mu) * (value - mu);

            const auto fx = boost::math::pdf(norm, value);

            result += w * ((-fx / sigma2) + (diff2 * fx / sigma4));
        }

        return result;
    }
}
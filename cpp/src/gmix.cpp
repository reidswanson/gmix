/*****************************************************************************
 * Copyright 2020 Reid Swanson
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*****************************************************************************/

#include <Python.h>

#include <vector>

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/module.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include <gmix/gmix.hpp>

namespace py = boost::python;

typedef std::vector<double> Vector;

// -- Utility methods ---------------------------------------------------------------------------//
template <typename A>
int tuple_length(const A&) {
    return std::tuple_size<A>::value;
}

template <int cidx, typename ... A>
typename std::enable_if<cidx >= sizeof...(A), py::object>::type
get_tuple_item_(const std::tuple<A...>& a, int idx, void* = nullptr) {
    throw std::out_of_range{"Ur outta range buddy"};
}

template <int cidx, typename ... A, typename = std::enable_if<(cidx < sizeof ...(A))>>
typename std::enable_if<cidx < sizeof...(A), py::object>::type
get_tuple_item_(const std::tuple<A...>& a, int idx, int = 42) {
    if (idx == cidx) {
        return py::object{std::get<cidx>(a)};
    } else {
        return get_tuple_item_<cidx+1>(a, idx);
    }
};

template <typename A>
py::object get_tuple_item(const A& a, int index) {
    return get_tuple_item_<0>(a, index);
}
// -- End Utility methods -----------------------------------------------------------------------//


double pdf(double x, const py::list &w, const py::list &m, const py::list &s) {
    Vector weights  = Vector(py::stl_input_iterator<double>(w), py::stl_input_iterator<double>());
    Vector means    = Vector(py::stl_input_iterator<double>(m), py::stl_input_iterator<double>());
    Vector sigmas   = Vector(py::stl_input_iterator<double>(s), py::stl_input_iterator<double>());

    return gmix::pdf(x, weights, means, sigmas);
}

/**
 *
 */
std::vector<std::tuple<double, double>> find_modes_newton(
    const py::list &w,
    const py::list &m,
    const py::list &s,
    const unsigned int max_itr,
    const double min_diff,
    const double min_grad
) {
    Vector weights  = Vector(py::stl_input_iterator<double>(w), py::stl_input_iterator<double>());
    Vector means    = Vector(py::stl_input_iterator<double>(m), py::stl_input_iterator<double>());
    Vector sigmas   = Vector(py::stl_input_iterator<double>(s), py::stl_input_iterator<double>());

    auto result = gmix::find_modes_newton(weights, means, sigmas, max_itr, min_diff, min_grad);

    return result;
}

BOOST_PYTHON_MODULE(cgmix) {
    Py_Initialize();

    using DoubleTuple = std::tuple<double, double>;

    py::class_<DoubleTuple>("TupleOfDoubles", py::init<double, double>())
        .def("__len__", &tuple_length<DoubleTuple>)
        .def("__getitem__", &get_tuple_item<DoubleTuple>);

    py::class_<std::vector<DoubleTuple>>("TupleVector")
        .def(py::vector_indexing_suite<std::vector<DoubleTuple>>());

    py::def("pdf", pdf);
    py::def("find_modes_newton", find_modes_newton);
}
